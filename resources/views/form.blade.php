<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>

<form action="/welcome" method="POST">
@csrf
 <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname"><br><br>
 
  <label for="gender">Gender:</label><br>
  	<input type="radio" id="male" name="gender" value="male">
  	<label for="male">Male</label><br>
  	<input type="radio" id="female" name="gender" value="female">
  	<label for="female">Female</label><br>
  	<input type="radio" id="other" name="gender" value="other">
  	<label for="other">Other</label><br><br>
  
  <label for="national">Nationality:</label>
  <select name="national" id="national">
  	<option value="indonesia">Indonesian</option>
  	<option value="malaysia">Malaysian</option>
  	<option value="singapore">Singaporean</option>
	<option value="australia">Australian</option>
  </select><br><br>

  <input type="checkbox" id="language1" name="language1" value="bahasa">
  <label for="language1"> Bahasa Indonesia</label><br>
  <input type="checkbox" id="language2" name="language2" value="english">
  <label for="language2"> English</label><br>
  <input type="checkbox" id="language3" name="language3" value="other">
  <label for="language3"> Other</label><br><br>

  <label for="bio">Bio:</label><br>
  <textarea id="bio" name="bio" rows="8" cols="30">

  </textarea>
  <br><br>

  <input type="submit" value="Sign Up">
</form>

</body>
</html>